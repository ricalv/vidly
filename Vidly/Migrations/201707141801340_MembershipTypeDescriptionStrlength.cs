namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MembershipTypeDescriptionStrlength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MembershipTypes", "Description", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MembershipTypes", "Description", c => c.String());
        }
    }
}
