namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'09e2e4a7-1f7c-40bf-942c-44fa35852b80', N'admin@vidly.com', 0, N'AD+QkukYLHJJrtOY0miHrW4EhUc0XIRXXUmZe9pugJ9+zDrFndHSjG++ICs7CmTFcQ==', N'67ca4c5c-6362-4e8f-8ad7-eac7a256550a', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ae825a2a-468b-43f5-876e-d0cae168c7b2', N'guest@vidly.com', 0, N'AG8bqwUaHrPJZdFZbE6GPsoMOdQRDSsgkd2aoAhqU7HVXNZzUaLfzLbsKjlzL31yqw==', N'0fdce146-d472-4764-bdae-d9f5ac3cd2fd', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'3f2c27ee-f612-4c29-97a7-9399459fc161', N'CanManageMovies')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'09e2e4a7-1f7c-40bf-942c-44fa35852b80', N'3f2c27ee-f612-4c29-97a7-9399459fc161')
");
        }
        
        public override void Down()
        {
        }
    }
}
