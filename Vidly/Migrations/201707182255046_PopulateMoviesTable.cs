namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMoviesTable : DbMigration
    {
        public override void Up()
        {
            Sql("Insert into Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) values ( 'Hangover', 1, '01/01/2000', '01/01/2000', 10, 5)");
            Sql("Insert into Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) values ( 'Die Hard', 2, '01/01/2000', '01/01/2000', 8, 4)");
            Sql("Insert into Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) values ( 'The Terminator', 2, '01/01/2000', '01/01/2000', 7, 6)");
            Sql("Insert into Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) values ( 'Toy Story', 3, '01/01/2000', '01/01/2000', 4, 4)");
            Sql("Insert into Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) values ( 'Titanic', 4, '01/01/2000', '01/01/2000', 6, 3)");
        }
        
        public override void Down()
        {
        }
    }
}
