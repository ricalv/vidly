namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateBirthDateToCustomers : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE customers set BirthDate = '01/01/1981' WHERE Id = 1");
        }
        
        public override void Down()
        {
        }
    }
}
