﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Min18YearsIfAMember : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var customer = (Customer)validationContext.ObjectInstance;

            if (customer.MembershipTypeId == MembershipType.Unknown || customer.MembershipTypeId == MembershipType.PayAsYouGo)
                return ValidationResult.Success;
            if (customer.Birthdate == null)
                return new ValidationResult("Año de Nacimiento es requerido");

            var age = DateTime.Today.Year - Convert.ToDateTime(customer.Birthdate).Year;

            return (age >= 18) 
                ? ValidationResult.Success 
                : new ValidationResult("El usuario debe tener al menos 18 años");
        }
    }
}